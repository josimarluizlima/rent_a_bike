import React from 'react';
import './App.css'
import { Container } from 'react-bootstrap';
import { Navbar, Nav } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import { Home } from './components/Home';
import { Cliente } from './components/Cliente';
import { Bicicleta } from './components/Bicicleta';
import { Plano } from './components/Plano';
import { Funciona } from './components/Funciona';
import { Contato } from './components/Contato';

function App() {
  return (
    <div className='tudo'>
      <BrowserRouter>
      <Navbar bg='dark' variant='dark' >
    <Navbar.Brand as={Link} to="/">Rent a Bike</Navbar.Brand>
    <Navbar.Toggle aria-controls='basic-navbar-nav' />
    <Navbar.Collapse id='basic-navbar-nav'>

<Nav className= "nav nav-tabs">
        <Nav.Link as={Link} to="/">Home</Nav.Link>
        <Nav.Link as={Link} to="/cliente">Cadastre-se</Nav.Link>
        <Nav.Link as={Link} to="/bicicleta">Bicicletas</Nav.Link>
        <Nav.Link as={Link} to="/plano">Planos</Nav.Link>
        <Nav.Link as={Link} to="/funciona">Como funciona?</Nav.Link>
        <Nav.Link as={Link} to="/contato">Contato</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
  <Switch>
  <Route path="/" exact={true} component={Home} />
  <Route path="/cliente" component={Cliente} />
  <Route path="/bicicleta" component={Bicicleta} />
  <Route path="/plano" component={Plano} />
  <Route path="/funciona" component={Funciona} />
  <Route path="/contato" component={Contato} />
  </Switch>
  
</BrowserRouter>
  
    </div>
  );
}
export default App;