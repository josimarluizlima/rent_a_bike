import React, { Component } from 'react';
import fundo from '../images/fundo.png'


export class Home extends Component {
  
  render() {
    
  
    return (
      

      <div> 
        <img src={fundo} className="App-fundo" alt="fundo" />
        <h1>Alugue uma bike agora mesmo e divirta-se!</h1>
          <p>Reserve sua diária aqui no site e garanta sua bike.</p>
      </div>
    );
  }
  
}
