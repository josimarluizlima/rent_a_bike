import React, { Component } from 'react';
import {Button, Form} from 'react-bootstrap';

export class Cliente extends Component {
    constructor(props) {
        super(props);
      
        this.state = {
          nome: '',
          cpf: '',
          nascimento: '',
          email: '',
          telefone1: '',
          telefone2: '',
          rua: '',
          numero: '',
          bairro: '',
          cidade: '',
          estado: ''
        };
      
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNomeChange = this.handleNomeChange.bind(this);
        this.handleCpfChange = this.handleCpfChange.bind(this);
        this.handleNascimentoChange = this.handleNascimentoChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleTelefone1Change = this.handleTelefone1Change.bind(this);
        this.handleTelefone2Change = this.handleTelefone2Change.bind(this);
        this.handleRuaChange = this.handleRuaChange.bind(this);
        this.handleNumeroChange = this.handleNumeroChange.bind(this);
        this.handleBairroChange = this.handleBairroChange.bind(this);
        this.handleCidadeChange = this.handleCidadeChange.bind(this);
        this.handleEstadoChange = this.handleEstadoChange.bind(this);

      }
      componentWillMount() {
        this.buscarCliente();
      }
    
      buscarCliente() {
        fetch('/api/clientes')
          .then(response => response.json())
          .then(data => this.setState({ Cliente: data }));
      }
    
      buscarCliente(id) {
        fetch('/api/clientes/' + id)
          .then(response => response.json())
          .then(data => this.setState(
            {
              id: data.id,
              descricao: data.cpf,
              prioridade: data.nome
            }));
      }
      inserirCliente(Cliente) {
        fetch('/api/cliente', {
          method: 'post',
          headers: {'Content-Type':'application/json'},
          body: JSON.stringify(Cliente)
        }).then((resposta) => {
          if (resposta.ok) {
            this.buscarCliente();
            this.cancelar();
          } else {
            alert(JSON.stringify(resposta));
          }
        });
      }
      atualizarCliente(Cliente) {
        fetch('/api/cliente/' + Cliente.id, {
          method: 'put',
          headers: {'Content-Type':'application/json'},
          body: JSON.stringify(Cliente)
        }).then((resposta) => {
          if (resposta.ok) {
            this.buscarCliente();
            this.cancelar();
          } else {
            alert(JSON.stringify(resposta));
          }
        });
      }
      excluirCliente(id) {
        fetch('/api/cliente/' + id, {
          method: 'delete'
        }).then((resposta) => {
          if (resposta.ok) {
            this.buscarCliente();
            this.cancelar();
          } else {
            alert(JSON.stringify(resposta));
          }
        });
      }
      handleSubmit(e) {
        alert('Cadastro realizado com sucesso: ' + JSON.stringify(this.state));
        this.setState({
          nome: '',
          cpf: '',
          nascimento: '',
          email: '',
          telefone1: '',
          telefone2: '',
          rua: '',
          numero: '',
          bairro: '',
          cidade: '',
          estado: ''

        });
        e.preventDefault();
      }
      
      handleNomeChange(e) {
        this.setState({
          nome: e.target.value
        });
      }
      handleCpfChange(e) {
        this.setState({
          cpf: e.target.value
        });
      }
      handleNascimentoChange(e) {
        this.setState({
          nascimento: e.target.value
        });
      }
      handleEmailChange(e) {
        this.setState({
          email: e.target.value
          
        });
      }
      handleTelefone1Change(e) {
        this.setState({
          telefone1: e.target.value
          
        });
      }
      handleTelefone2Change(e) {
        this.setState({
          telefone2: e.target.value
          
        });
      }
      
      handleRuaChange(e) {
        this.setState({
          rua: e.target.value
        });
      }
      handleNumeroChange(e) {
        this.setState({
          numero: e.target.value
        });
      }
      handleBairroChange(e) {
        this.setState({
          bairro: e.target.value
        });
      }
      handleCidadeChange(e) {
        this.setState({
          cidade: e.target.value
        });
      }
      handleEstadoChange(e) {
        this.setState({
          estado: e.target.value
        });
      }
      
  render() {
    return (
      <div>
          <Form onSubmit={this.handleSubmit}>
  <Form.Group>
    <Form.Label>Nome</Form.Label>
    <Form.Control type='text' placeholder='Insira seu nome completo aqui' value={this.state.nome} onChange={this.handleNomeChange} />
  </Form.Group>
  <Form.Group>
    <Form.Label>Documentos</Form.Label>
    <Form.Control type='number' placeholder='CPF' value={this.state.cpf} onChange={this.handleCpfChange} />
    <Form.Control type='number' placeholder='Nascimento' value={this.state.nascimento} onChange={this.handleNascimentoChange} />
  </Form.Group>
  <Form.Group>
    <Form.Label>Contato</Form.Label>
    <Form.Control type='text' placeholder='Email' value={this.state.email} onChange={this.handleEmailChange} />
    <Form.Control type='number' placeholder='Telefone 1' value={this.state.telefone1} onChange={this.handleTelefone1Change} />
    <Form.Control type='number' placeholder='Telefone 2' value={this.state.telefone2} onChange={this.handleTelefone2Change} />
  </Form.Group>
  <Form.Group>
    <Form.Label>Endereço</Form.Label>
    <Form.Control type='text' placeholder='Rua' value={this.state.rua} onChange={this.handleRuaChange} />
    <Form.Control type='number' placeholder='Numero' value={this.state.numero} onChange={this.handleNumeroChange} />
    <Form.Control type='text' placeholder='Bairro' value={this.state.bairro} onChange={this.handleBairroChange} />
    <Form.Control type='text' placeholder='Cidade' value={this.state.cidade} onChange={this.handleCidadeChange} />
    <Form.Control type='text' placeholder='Estado' value={this.state.estado} onChange={this.handleEstadoChange} />
  </Form.Group>
  <Button variant='primary' type='submit'>
    Cadastrar
  </Button>
  <Button variant="secondary" onClick={this.cancelar}>
    Cancelar
  </Button>

</Form>

      </div>
    );
  }
}


